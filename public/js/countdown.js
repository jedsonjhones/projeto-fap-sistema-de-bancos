document.addEventListener('DOMContentLoaded', function () {
    let seconds = 5;

    const countdownElement = document.getElementById('countdown');

    function updateCountdown() {
        countdownElement.textContent = seconds;
        seconds--;

        if (seconds < 0) {
            window.location.href = '/login-master';
        } else {
            setTimeout(updateCountdown, 1000);
        }
    }

    updateCountdown();
});
