document.addEventListener('DOMContentLoaded', function () {
    // Pie chart
    new Chart(document.getElementById('chartjs-dashboard-pie'), {
        type: 'pie',
        data: {
            labels: ['Ativos', 'Inadiplentes', 'A definir'],
            datasets: [{
                data: [5, 2, 1],
                backgroundColor: [
                    window.theme.success,
                    window.theme.warning,
                    window.theme.danger
                ],
                borderWidth: 5
            }]
        },
        options: {
            responsive: !window.MSInputMethodContext,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            cutoutPercentage: 75
        }
    });
});

