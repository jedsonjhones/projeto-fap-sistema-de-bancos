const Sequelize = require('sequelize');
const dbConfig = require('../config/database');
const UserMaster = require('../models/UserMaster');
const {Usuario,Conta,Cartao} = require('../models/client/user');
const connection = new Sequelize(dbConfig);

Usuario.init(connection);
Conta.init(connection);
Cartao.init(connection);
UserMaster.init(connection);

module.exports = connection;

