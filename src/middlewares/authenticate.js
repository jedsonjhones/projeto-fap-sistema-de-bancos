const { Conta, Cartao } = require('../infra/settings');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const {verificarAssinaturaDigital} = require('../infra/auxiliar/verificaAssinatura');
const bcrypt =  require('bcryptjs');
dotenv.config();


module.exports = {async auth(req, resp, next){
    try {
        const { dados, senha } = req.body;
        let vencimento = new Date(dados.vencimento).getTime();
        console.log(senha);
        if(verificarAssinaturaDigital(req, resp, next) && vencimento>Date.now()){
            
            const cartao = await Cartao.findByPk(dados.pam);
            if (cartao) {
                if(cartao.status == 'ativo'){
                    const conta = await Conta.findOne({where:{n_conta:cartao.conta_n_conta,agencia:cartao.conta_agencia}});
                    console.log(conta);
                    if(3>conta.tentativa){
                        console.log(cartao.senha);
                        console.log(senha);
                        console.log(bcrypt.compareSync(senha, cartao.senha));
                        if(bcrypt.compareSync(senha, cartao.senha)){
                            console.log('quase funcionou');
                            const token = jwt.sign({ sub: cartao.usuario_id,ag:cartao.conta_agencia,ct:cartao.conta_n_conta}, 'chaveSecreta', {
                                expiresIn: '1h' // Defina a expiração do token como apropriado
                            });
                            
                            resp.json({ token });

                        }
                        else{
                            conta.tentativa += 1;
                            if (conta.tentativa>=3){
                                conta.status = 'bloqueada';
                            }
                            await conta.save();
                            resp.status(404).json({ error: 'Senha incorreta.' });

                        }
                    }else{
                        resp.status(404).json({ error: 'Conta bloqueada' });
                    }
                } else {
                    resp.status(404).json({ error: 'Cartao não encontrado.' });
                }
            }else{
                resp.status(404).json({ error: 'Cartao invalido.' });
            }
        }
        else{
            resp.status(404).json({ error: 'Erro ao identificar integridade' });
        }
    } catch (error) {
        console.error(error);
        resp.status(500).json({ error: 'Erro ao gerar token.' });
    }
}};
