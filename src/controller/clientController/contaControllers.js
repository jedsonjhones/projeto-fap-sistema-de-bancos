const { ContaHC } = require('../../infra/repository/contaRepository.js');
const {Conta,UsuarioHasConta} = require('../../infra/settings.js');

const getContasBy = (req, resp) => {
    const contaRepository = new ContaHC(Conta);
    contaRepository.getContasBy(req, resp);
    console.log(resp);
};

const getAllContas = (req, resp) => {
    const contaRepository = new ContaHC(Conta,UsuarioHasConta);
    contaRepository.getAllContas(req, resp);
    
};
  
const createConta = (req, resp) => {
    const userRepository = new ContaHC(Conta,UsuarioHasConta);
    userRepository.insertConta(req, resp);
    
};


    
module.exports = {
      
    getContasBy,
    getAllContas,
    createConta
};