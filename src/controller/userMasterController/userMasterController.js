const User = require('../../models/UserMaster');
const { Usuario,Conta,Cartao } = require('../../models/client/user');
const bcrypt = require('bcrypt');
module.exports = {
    async registerUser(req,res){
        try {
            
            const { name, email, password } = req.body;
            const hashedPassword =  await bcrypt.hash(password, 10);
        
            const user = await User.create({
                name,
                email,
                password: hashedPassword
            });
        
            res.status(201).json({ user });
        } catch (error) {
            res.status(500).json({ error: error.message });
        }
    },
    async updateUserMaster(req, res) {
        try {
            const { id } = req.params;
            const { name, email, password } = req.body;
    
            const user = await User.findByPk(id);
    
            if (!user) {
                return res.status(404).json({ error: 'Usuário não encontrado' });
            }
    
            await user.update({ name, email, password });
    
            res.redirect('/dashboard');
        } catch (error) {
            console.error(error);
            req.session.errorMessage = 'Erro ao editar usuário';
            return res.redirect('/error-popup');
            
        }
    },

    async cadastrarUsuario(req, res) {
        try {
            const { nome, cpf, estado, cidade, bairro, rua, numero, telefone, status, email, complemento, senha } = req.body;

            const hashedPassword = await bcrypt.hash(senha, 10);
    
            const novoUsuario = await Usuario.create({
                nome,
                cpf,
                estado,
                cidade,
                bairro,
                rua,
                numero,
                telefone,
                status,
                email,
                complemento,
                senha: hashedPassword,
            });
    
            res.redirect('/dashboard');
        } catch (error) {
            req.session.errorMessage = 'Erro ao cadastrar usuário';
            return res.redirect('/error-popup');
        }

    },
    async createCard(req, res) {
        try {
            const { pam, emissao, tipo, categoria, vencimento } = req.body;
            const cartao = await Cartao.create({ pam, emissao, tipo, categoria, vencimento });
            res.redirect('/dashboard');
        } catch (error) {
            req.session.errorMessage = 'Erro ao criar cartão';
            return res.redirect('/error-popup');
        }
    },

    async createAccount(req, res) {
        try {
            const { n_conta, tipo, status, saldo, agencia,} = req.body;
            const conta = await Conta.create({ n_conta, tipo, status, saldo, agencia,});
            res.redirect('/dashboard');
        } catch (error) {
            req.session.errorMessage = 'Erro ao criar conta';
            return res.redirect('/error-popup');
        }
    },
    async renderUpdateUser(req, res) {
        try {
            const user = await User.findAll();

            res.render('UserMaster/pages-user-update',{user});
        } catch (error) {
            console.error('Erro ao buscar usuários:', error);
            res.status(500).render('UserMaster/pages-sign-in-master');
        }
    },
    renderCadastrarConta(req, res) {
        res.render('UserMaster/pages-acount-create');
    },
    renderCadastrarCartao(req, res) {
        res.render('UserMaster/pages-cartao-create');
    },

    renderCadastrarUsuario(req, res) {
        res.render('UserMaster/pages-register-user');
    },
    profile(req,res){
        res.render('UserMaster/pages-profile',); 
    }
};