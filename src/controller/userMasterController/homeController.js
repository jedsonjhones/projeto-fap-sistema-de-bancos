const { Usuario } = require('../../models/client/user');
function getStatusClass(status) {
    switch (status) {
    case 'ativo':
        return 'badge bg-success';
    case 'cancelado':
        return 'badge bg-danger';
    case 'inadimplente':
        return 'badge bg-warning';
    default:
        return 'badge bg-secondary';
    }
}
module.exports = { 
    async home(req, res) {
        try {
            const usuarios = await Usuario.findAll();
            const ativos = usuarios.filter(usuario => usuario.status === 'ativo').length;
       
            const inadimplentes = usuarios.filter(usuario => usuario.status === 'inadimplente').length;
            const cancelados = usuarios.filter(usuario => usuario.status === 'cancelado').length;

            res.render('UserMaster/index', { usuarios,ativos,inadimplentes,cancelados,getStatusClass });
        } catch (error) {
            console.error('Erro ao buscar usuários:', error);
            res.status(500).render('UserMaster/pages-sign-in-master');
        }
    }
};

