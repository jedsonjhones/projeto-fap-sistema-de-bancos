const express = require('express');

const routerUserMaster = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const userMasterController = require('../controller/userMasterController/userMasterController');
const logincontroller = require('../controller/userMasterController/loginController');
const homeController = require('../controller/userMasterController/homeController');

routerUserMaster.post('/login-master', logincontroller.loginUser);
routerUserMaster.post('/register', userMasterController.registerUser);
routerUserMaster.post('/verificar-2fa', logincontroller.verifyTwoFactor);

routerUserMaster.post('/cadastrar-usuario', userMasterController.cadastrarUsuario);
routerUserMaster.post('/register-card', userMasterController.createCard);
routerUserMaster.post('/register-acount', userMasterController.createAccount);
routerUserMaster.post('/update-user/:id', userMasterController.updateUserMaster);

routerUserMaster.get('/login-master', logincontroller.loginRender);
routerUserMaster.get('/logout-master', logincontroller.logout);
routerUserMaster.get('/dashboard',authMiddleware.protected, homeController.home);
routerUserMaster.get('/profile',authMiddleware.protected, userMasterController.profile);
routerUserMaster.get('/update-user',authMiddleware.protected, userMasterController.renderUpdateUser);

routerUserMaster.get('/error-popup', logincontroller.errorPopupRender);


routerUserMaster.get('/cadastrar-usuario', userMasterController.renderCadastrarUsuario);
routerUserMaster.get('/register-card', userMasterController.renderCadastrarCartao);
routerUserMaster.get('/register-acount', userMasterController.renderCadastrarConta);





  



module.exports = routerUserMaster;