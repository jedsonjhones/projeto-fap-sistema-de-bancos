const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const { generateConta } = require('../auxiliar/generateAccount');

dotenv.config();



class ContaHC{
    constructor(conta,hasConta){
        this.conta = conta;
        this.hasConta = hasConta;

    }

    async getAllContas(req, resp){
        try {
            const contas = await this.conta.findAll();
            resp.status(200).json(contas);
        } catch (error) {
            // Lidar com erros de consulta, se houver
            console.error(error);
            resp.status(500).json({ error: 'Erro ao buscar Contas.' });
        }
    }
    async getContasBy(req, resp){
        try {
            const {tipo,valor} = req.body;
            const contas = await this.conta.findAll({where:{[tipo]:valor}});
            resp.status(200).json(contas);
        } catch (error) {
            // Lidar com erros de consulta, se houver
            console.error(error);
            resp.status(500).json({ error: 'Erro ao buscar contas.' });
        }
    }

    async getContaById(req, resp) {
        try {
            const {n_contaValue,
                agenciaValue,
            } = req.body;
            const conta = await this.conta.findone({where:{n_conta:n_contaValue,agencia:agenciaValue}});
            if (conta) {
                resp.status(200).json(conta);
            } else {
                resp.status(404).json({ error: 'Conta não encontrada.' });
            }
        } catch (error) {
            console.error(error);
            resp.status(500).json({ error: 'Erro ao buscar a conta.' });
        }
    }

    async insertConta(req, resp) {
        const transaction = await sequelize.transaction();

        try {
            const 
                {idValue,
                    tipoValue,
                    senhaValue,
      
                    statusValue,
                } = req.body;
            const contaValue = generateConta(idValue);
            console.log(contaValue);
            const newConta  = await this.conta.create({
                n_conta: contaValue,
                tipo: tipoValue,
                status: statusValue,
                agencia: 1,
                senha: bcrypt.hashSync(senhaValue, bcrypt.genSaltSync(10))
            },
            { transaction }
            );


            const newLink = await this.hasConta.create({
                usuario_id :idValue,
                conta_n_conta:contaValue,
                conta_agencia:1 
            },
            { transaction }
            );

            await transaction.commit();
            resp.status(200).json({newConta,newLink});
      
      
        } catch (error) {
            await transaction.rollback();
            console.error(error);
            resp.status(500).json({ error: 'Erro ao buscar o Conta.' });
        }

    
    }

    async updateConta(req, resp) {
        try {
            const id = parseInt(req.params.id);
            const 
                {tipoValue,
                    senhaValue,
                    saldoValue,
      
                    statusValue,
                } = req.body;

            const conta = await this.conta.findByPk(id);
      
            conta.tipo = tipoValue;
            conta.senha = bcrypt.hashSync(senhaValue, bcrypt.genSaltSync(10));
            conta.saldo = saldoValue;
            conta.status = statusValue;
      
            await conta.save();
      
            if (conta) {
                resp.status(200).json(conta);
                await conta.save();
            } else {
                resp.status(404).json({ error: 'conta não encontrada.' });
            }
        } catch (error) {
    
            resp.status(500).json({ error: 'Erro ao buscar conta.' });
        }

    }

    async deposito(req, resp){
        try{
            const {contaValue, depositoValue, agenciaValue} = req.body;

            const rendimento = await this.rendimento.findOne({
                where: {
                    [Op.and]: [
                        { n_conta: contaValue },
                        { agencia: agenciaValue }
                    ]
                }
            });
            if(rendimento){
                rendimento.saldo = parseFloat(rendimento.saldo)+depositoValue;
                rendimento.save();
                resp.status(200).json(conta);
            }
            else{
                resp.status(404).json({ error: 'conta não encontrada.' });
            }
      
        }catch(error){
            resp.status(500).json({ error: 'Erro ao fazer deposito.' });
        }
    }

    async saque(req, resp){
        try{
            const {contaValue, saqueValue, agenciaValue} = req.body;

            const rendimento = await this.rendimento.findOne({
                where: {
                    [Op.and]: [
                        { n_conta: contaValue },
                        { agencia: agenciaValue }
                    ]
                }
            });
            if(rendimento){
                if(parseFloat(rendimento.saldo)>=saqueValue){
                    rendimento.saldo = parseFloat(rendimento.saldo)-saqueValue;
                    rendimento.save();
                    resp.status(200).json(conta);

                }
                else{
                    resp.status(500).json({ error: 'saldo insuficiente.' });
                }
      
            }
            else{
                resp.status(404).json({ error: 'conta não encontrada.' });
            }
    
        }catch(error){
            resp.status(500).json({ error: 'Erro ao fazer saque.' });
        }
    }


    async  deleteConta(req, resp) {
        try {
            const id = parseInt(req.params.id);
            const conta = await this.conta.findByPk(id);
  
            if (!conta) {
                return resp.status(404).send(`Conta with ID: ${id} not found`);
            }
  
            await conta.destroy();
  
            resp.status(200).send(`User deleted with ID: ${id}`);
        } catch (error) {
            console.error(error);
            resp.status(500).send('Error deleting account');
        }
    }

}

module.exports = {ContaHC};