const crypto = require('crypto');



  
module.exports = {async verificarAssinaturaDigital(req, res, next) {
    const { dados, assinatura } = req.body;
    const hmac = crypto.createHmac('sha256', 'wqrteq21245ys#d'); // chave secreta deve ser definida e conhecida pelo cartão e banco de dados
    const dadosString = JSON.stringify(dados);
    const assinaturaCalculada = hmac.update(dadosString).digest('hex');
  
    return assinatura === assinaturaCalculada;
}}